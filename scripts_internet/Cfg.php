<?php
namespace ScriptsSC;

class Cfg {
    // PHPMailer
    const PHPMAILER_HOST = '10.40.20.5';
    const PHPMAILER_SMTP_AUTH = false;
    const PHPMAILER_USERNAME = '';
    const PHPMAILER_SMTP_SECURE = 'tls';
    const PHPMAILER_PORT = 25;
    const PHPMAILER_CHARSET = 'UTF-8';
    const PHPMAILER_DEFAULT_FROM_EMAIL = 'robot@nember.cz';
    const PHPMAILER_DEFAULT_FROM_NAME = 'Robot';
    const PHPMAILER_INFO_RECIPIENTS = 'it@nember.cz';
    const PHPMAILER_EXCEPTION_RECIPIENTS = 'it@nember.cz';
    const PHPMAILER_SMTP_OPTIONS = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ],
        ];
}
?>