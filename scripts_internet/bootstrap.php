<?php
    //Load composer's autoloader
    require_once 'vendor/autoload.php';
    
    require_once 'Cfg.php';
    require_once 'CommonFunc.php';
    require_once 'FileLog.php';
    require_once 'LangUtils.php';
    require_once 'connector/PgSQLConnector.php';
    require_once 'connector/PgSQLPDOConnector.php';
    require_once 'connector/MSSQLConnector.php';
    require_once 'connector/MSSQLPDOConnector.php';
    require_once 'connector/MySQLConnector.php';
    require_once 'connector/FbSQLConnector.php';
    require_once 'PDOQueryParametersHelper.php';
    require_once 'CustomDatabaseConnector.php';
    require_once 'PHPMailerWrapper.php';
    require_once 'Renderer.php';
?>