<?php
namespace ScriptsSC;

/**
 * Renderování šablony
 */
class Renderer {
    private $latte;
    private $params = array();
    private $template = __DIR__ . '/../templates/layout.latte';

    public function __construct() {
        $this->latte = new \Latte\Engine;
        $this->latte->setTempDirectory(NULL);
    }

    /**
     * Nastaví layout
     * @param string $layout název layoutu
     */
    public function setLayout($layout) {
        $this->template = __DIR__ . '/../templates/' . $layout . '.latte';
        $this->params['layout'] = $layout;
    }

    /**
     * Přidá parametr pro šablonu
     * @param type $paramName název parametru
     * @param type $paramValue hodnota parametru
     */
    public function addParam($paramName, $paramValue) {
        $this->params[$paramName] = $paramValue;
    }

    /**
     * Začne zachytávat výstup do bufferu pro jeho pozdejší uložení jako část stránky
     */
    public function contentStart() {
        ob_start();
    }

    /**
     * Uloží výstup zachycený do bufferu jako část stránky
     * @param string $name název části stránky
     */
    public function contentSave($name) {
        $content = ob_get_contents();
        ob_clean();

        $this->params[$name] = $content;
    }

    /**
     * Vyrenderuje šablonu
     */
    public function render() {
        $this->latte->render($this->template, $this->params);
    }
}
?>