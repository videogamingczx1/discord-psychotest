
# Discord Psychotesty

## Datový model

#### Uzivatele
| Atribut       | Typ      |
|---------------|----------|
| uzivatel_id   | int      |
| admin         | boolean  |
| aktivni       | boolean  |
| login         | varchar  |
| heslo         | varchar  |
| jmeno         | varchar  |
| vytvoreno     | datetime |

#### Vysledky
| Atribut     | Typ      |
|-------------|----------|
| vysledek_id | int      |
| uzivatel_id | int      |
| cas         | datetime |

#### Vysledky_Odpovedi
| Atribut     | Typ    |
|-------------|--------|
| vysledek_id | int    |
| otazka_id   | int    |
| odpoved     | text   |

#### Kategorie
| Atribut          | Typ    |
|------------------|--------|
| kategorie_id     | int    |
| nazev_kategorie  | varchar|
| popis_kategorie  | text   |

#### Otazky
| Atribut        | Typ      |
|----------------|----------|
| otazka_id      | int      |
| kategorie_id   | int      |
| poradi         | int      |
| zneni_otazky   | text     |

## Charakteristika

### Admin
Admin může založit nové uživatelské účty, přiřazuje jméno, login, heslo a určuje, zda bude účet správcem nebo standardním uživatelem.
Může také prohlížet výsledky testů, filtrovat je podle uživatele a procházet detailní odpovědi.
Aplikace bude poskytovat adminovi statistický přehled založený na výsledcích testů.


### Uživatel
Uživatelé se přihlašují pomocí svého loginu a hesla. Poté vyplňují testy složené z různých kategorií a otázek. Po dokončení testu se uživatelův účet automaticky deaktivuje. Do budoucna mě ještě napadá přidat, jaké role/práva budou uživateli po vyplnění testu zobrazeny na základě zvolených odpovědí.

## Specifikace uživatelských rolí a oprávnění
Jak již bylo psáno v charakteristice, tak admin spravuje uživatelské účty, vytváří nové účty, nastavuje role a hesla. Má přístup ke všem výsledkům testů a statistikám aplikace, může prohlížet a filtrovat výsledky dle potřeby. 
Uživatel se přihlašuje do systému, vyplňuje testy a po jejich dokončení je účet automaticky deaktivován.


## Uživatelské grafické rozhraní
Aplikace bude ve stylu Discordu, s důrazem na přehlednost, intuitivní navigaci a známé vizuální prvky charakteristické pro Discord.

## Datový logický model

### Tabulky a jejich Atributy

#### Uzivatele
- uzivatel_id (primární klíč)
- admin (boolean)
- aktivni (boolean)
- login (varchar)
- heslo (varchar)
- jmeno (varchar)
- vytvoreno (datetime)

#### Vysledky
- vysledek_id (primární klíč)
- uzivatel_id (cizí klíč z Uzivatele)
- cas (datetime)

#### Vysledky_Odpovedi
- vysledek_id (cizí klíč z Vysledky)
- otazka_id (cizí klíč z Otazky)
- odpoved (text)

#### Kategorie
- kategorie_id (primární klíč)
- nazev_kategorie (varchar)
- popis_kategorie (text)

#### Otazky
- otazka_id (primární klíč)
- kategorie_id (cizí klíč z Kategorie)
- poradi (int)
- zneni_otazky (text)

### Vztahy mezi Tabulkami
- Uzivatele a Vysledky: Vztah 1:N. Každý uživatel může mít více výsledků testů, ale každý výsledek patří právě jednomu uživateli.
- Vysledky a Vysledky_Odpovedi: Vztah 1:N. Každý výsledek může obsahovat více odpovědí, ale každá odpověď je svázána s jedním výsledkem.
- Otazky a Vysledky_Odpovedi: Vztah 1:N. Každá otázka může být použita v mnoha odpovědích, ale každá odpověď odpovídá na jednu konkrétní otázku.
- Kategorie a Otazky: Vztah 1:N. Každá kategorie může obsahovat více otázek, ale každá otázka patří do jedné specifické kategorie.

## Klíčové metody
- **actionSaveTest**: Ukládá odpovědi uživatele do databáze a řídí databázovou transakci.
- **actionCheckTest**: Ověřuje, že odpovědi odpovídají otázkám a jsou úplné.
- **actionSend**: Zpracovává přihlášení uživatele, ověřuje heslo a spravuje session.
- **loadData**: Načítá data pro statistické zpracování odpovědí.
- **getStatistika**: Vrací zpracovaná statistická data.
- **exportData**: Exportuje data do CSV pro analýzy nebo reporty.
- **getAdministratori**: Poskytuje seznam administrátorů.
- **getPacientiOtevrenyUcet**: Získává seznam uživatelů s otevřenými účty.
- **getPacientiUzavrenyUcet**: Získává seznam uživatelů s uzavřenými účty.
- **loadVysledkyForSelect**: Načítá výsledky pro výběrové prvky uživatelského rozhraní.
- **getVysledkyForSelect**: Vrací výsledky pro výběrové prvky.
- **getVysledek**: Poskytuje konkrétní výsledek testu.

## Použité Technologie
- **PHP**: serverový jazyk pro zpracování požadavků a interakci s databází.
- **Postgres**: databáze pro ukládání uživatelských dat, výsledků testů a otázek.
- **HTML/CSS/JS**: tvorba uživatelského rozhraní.
- **Latte**: odděluje logiku aplikace od HTML kódu.
- **Scripts_internet**: Soubor skriptů pro připojení a manipulaci s databází.

### Vztahy mezi Tabulkami
- Uzivatele a Vysledky: Vztah 1:N. Každý uživatel může mít více výsledků testů, ale každý výsledek patří právě jednomu uživateli.
- Vysledky a Vysledky_Odpovedi: Vztah 1:N. Každý výsledek může obsahovat více odpovědí, ale každá odpověď je svázána s jedním výsledkem.
- Otazky a Vysledky_Odpovedi: Vztah 1:N. Každá otázka může být použita v mnoha odpovědích, ale každá odpověď odpovídá na jednu konkrétní otázku.
- Kategorie a Otazky: Vztah 1:N. Každá kategorie může obsahovat více otázek, ale každá otázka patří do jedné specifické kategorie.
