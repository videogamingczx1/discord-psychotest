<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/ADiscordPresenter.php';

class StatistikaPresenter extends ADiscordPresenter
{
    protected $statistika;
    
    public function __construct() {
        $this->template = 'statistika';
        
        $dbCor = $this->getOpenDatabaseConnection();
        
        $this->loadKategorieAndOtazky($dbCor);
        
        $this->loadData($dbCor);
        
        $dbCor->closeConnection();
    }
    
    public function loadData($dbCor) {
        $odpovedi = $dbCor->executeQuery('SELECT * FROM vysledky_odpovedi');
        
        $statistika = [];
        
        foreach($odpovedi as $odpoved) {
            if(!isset($statistika[$odpoved['otazka_id']])) {
                $statistika[$odpoved['otazka_id']] = ['hodnoty' => []];
            }
            $statistika[$odpoved['otazka_id']]['hodnoty'][] = intval($odpoved['odpoved']);
        }
        
        foreach($statistika as &$otazkaStatistika) {
            sort($otazkaStatistika['hodnoty']);
            $otazkaStatistika['min'] = min($otazkaStatistika['hodnoty']);
            $otazkaStatistika['max'] = max($otazkaStatistika['hodnoty']);
            $otazkaStatistika['prumer'] = array_sum($otazkaStatistika['hodnoty']) / sizeof($otazkaStatistika['hodnoty']);
            $otazkaStatistika['median'] = $otazkaStatistika['hodnoty'][floor((sizeof($otazkaStatistika['hodnoty']) - 1) / 2)];
            $otazkaStatistika['pocet'] = sizeof($otazkaStatistika['hodnoty']);
            $otazkaStatistika['pocty'] = array_count_values($otazkaStatistika['hodnoty']);
            $otazkaStatistika['procenta'] = [];
            foreach($otazkaStatistika['pocty'] as $odpoved => $pocet) {
                $otazkaStatistika['procenta'][$odpoved] = $pocet / $otazkaStatistika['pocet'];
            }
        }
        
        $this->statistika = $statistika;
    }
    
    public function getStatistika() {
        return $this->statistika;
    }
    
 
    
    public function render() {
        if(CommonFunc::safeGET('akce')=='exp') {
          //
        } else {
            parent::render();
        }
    }
}
