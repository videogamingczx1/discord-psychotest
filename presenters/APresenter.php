<?php
namespace DiscordPsychotesty;

abstract class APresenter
{
    protected $messages = [];
    
    protected function getOpenDatabaseConnection() {
        return App::getDatabaseConnection();
    }
    
    protected function addMessage($type, $message) {
        $this->messages[] = [$type, $message];
    }
    
    public function getMessages() {
        return $this->messages;
    }
    
    public function getCurrentURL() {
        return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') || strpos($_SERVER['HTTP_REFERER'], 'https://')===0 ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
}
