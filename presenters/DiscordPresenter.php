<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/ADiscordPresenter.php';

class DiscordPresenter extends ADiscordPresenter
{
    public $vysledek;
    public $kategorie;
    public $otazky;
    public function __construct() {
        $actionSave = CommonFunc::safePOST('save');
        
        $this->template = 'distest';
        
        $dbCor = $this->getOpenDatabaseConnection();
        
        if($actionSave) {
            if($this->actionCheckDiscord($dbCor)) {
                if($this->actionSaveDiscord($dbCor)) {
                    $dbCor->executeQuery('UPDATE uzivatele SET aktivni = false WHERE uzivatel_id = :uzivatel_id AND admin = false', ['uzivatel_id' => App::getUzivatelId()]);
                    
                    //App::logout(false);
                    
                    $uzivatelId = App::getUzivatelId(); 
                    $this->loadVysledkyOdpovediByUzivatelId($dbCor, $uzivatelId);
                      $this->vyhodnotOpravneni();
                    $this->template = 'prava';
                } else {
                    $this->addMessage('danger', 'Dotazník se nepodařilo uložit');
                }
            } else {
                $this->addMessage('danger', 'Dotazník není správně vyplněn');
            }
        }
        
        if($this->template=='distest') {
            $this->loadKategorieAndOtazky($dbCor);
        }
        
        $dbCor->closeConnection();
    }
    
    public function getPOSTDiscordData() {
        return CommonFunc::safePOSTArray('otazka');
    }


    public function loadVysledkyOdpovediByUzivatelId($dbCor, $uzivatelId) {
        $sql = "SELECT vo.otazka_id, vo.odpoved
            FROM public.vysledky_odpovedi AS vo
            JOIN public.vysledky AS v ON vo.vysledek_id = v.vysledek_id
            WHERE v.uzivatel_id = :uzivatel_id";

        $vysledkyOdpovedi = $dbCor->executeQuery($sql, ['uzivatel_id' => $uzivatelId]);
        
        $this->vysledek = [];
        
        foreach($vysledkyOdpovedi as $odpoved) {
            $this->vysledek[$odpoved['otazka_id']] = $odpoved['odpoved'];
        }
    }

    public function loadKategorieOtazkyVysledky($dbCor, $uzivatelId) {

        $this->kategorie = $dbCor->executeQuery('SELECT * FROM kategorie ORDER BY poradi');

        $otazky = $dbCor->executeQuery('SELECT * FROM otazky ORDER BY kategorie_id, poradi');
        $this->otazky = [];
        foreach ($otazky as $otazka) {
            $this->otazky[$otazka['kategorie_id']][] = $otazka;
        }

        $this->loadVysledkyOdpovediByUzivatelId($dbCor, $uzivatelId);
    }

    public function getVysledek() {
        return $this->vysledek;
    }
    
    public function actionCheckDiscord($dbCor) {
        $otazkyDb = $dbCor->executeQuery('SELECT * FROM otazky ORDER BY kategorie_id, poradi');
        
        $otazkyDbById = [];
        
        foreach($otazkyDb as $otazkaDb) {
            $otazkyDbById[$otazkaDb['otazka_id']][] = $otazkaDb;
        }
        
        $otazky = CommonFunc::safePOSTArray('otazka');
        
        foreach($otazky as $otazkaId => $odpoved) {
            if(!isset($otazkyDbById[$otazkaId])) {
                return false;
            }
            unset($otazkyDbById[$otazkaId]);
        }

        
        
        if(!empty($otazkyDbById)) {
            return false;
        }
        
        return true;
    }
    
    public function actionSaveDiscord($dbCor) {
        $dbCor->beginTransaction();
        
        $dbCor->executeQuery('INSERT INTO vysledky (uzivatel_id) VALUES (:uzivatel_id)', ['uzivatel_id' => App::getUzivatelId()]);
        $vysledekIdResult = $dbCor->executeQuery('SELECT MAX(vysledek_id) AS vysledek_id FROM vysledky WHERE uzivatel_id = :uzivatel_id', ['uzivatel_id' => App::getUzivatelId()]);
        
        if($vysledekIdResult) {
            $vysledekId = $vysledekIdResult[0]['vysledek_id'];
            
            $otazky = CommonFunc::safePOSTArray('otazka');
            
            foreach($otazky as $otazkaId => $odpoved) {
                $dbCor->executeQuery('INSERT INTO vysledky_odpovedi (vysledek_id, otazka_id, odpoved) VALUES (:vysledek_id, :otazka_id, :odpoved)',
                        ['vysledek_id' => $vysledekId, 'otazka_id' => $otazkaId, 'odpoved' => $odpoved]);
            }
            
            $dbCor->commitTransaction();
            return true;
        } else {
            $dbCor->rollbackTransaction();
            return false;
        }
    }
}
