<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/APresenter.php';

class UzivatelePresenter extends APresenter
{
    protected $uzivatelData;
    protected $administratori;
    protected $pacientiOtevrenyUcet;
    protected $pacientiUzavrenyUcet;
    
    public function __construct() {
        $dbCor = $this->getOpenDatabaseConnection();
        
        $actionNew = CommonFunc::safePOST('new');
        
        if($actionNew) {
            $this->actionNew($dbCor);
        }
        
        $actionDelete = CommonFunc::safePOSTArray('delete');
        
        if($actionDelete) {
            $this->actionDelete($actionDelete, $dbCor);
        }
        
        $actionLoadEdit = CommonFunc::safePOSTArray('load-edit');
        
        if($actionLoadEdit) {
            $this->actionLoadEdit($actionLoadEdit, $dbCor);
        }
        
        $actionEdit = CommonFunc::safePOSTArray('edit');
        
        if($actionEdit) {
            $this->actionEdit($actionEdit, $dbCor);
        }
        
        $this->loadUzivatele($dbCor);
        
        $dbCor->closeConnection();
    }
    
    protected function actionNew($dbCor) {
        $admin = CommonFunc::safePOST('admin');
        $login = CommonFunc::safePOST('login');
        $heslo = CommonFunc::safePOST('heslo');
        $jmeno = CommonFunc::safePOST('jmeno');
        $datumNarozeni = CommonFunc::safePOST('datum_narozeni');
        $datumNarozeni = $datumNarozeni ? (new \DateTime(str_replace(' ', '', $datumNarozeni)))->format('Y-m-d') : null;
        
        if($login && $heslo && $jmeno) {
            $hesloHash = password_hash(CommonFunc::safePOST('heslo'), PASSWORD_BCRYPT);
            
            $result = $dbCor->executeQuery('INSERT INTO uzivatele (admin, login, heslo, jmeno, datum_narozeni) VALUES (:admin, :login, :heslo, :jmeno, :datum_narozeni)',
                    ['admin' => $admin, 'login' => $login, 'heslo' => $hesloHash, 'jmeno' => $jmeno, 'datum_narozeni' => $datumNarozeni]);

            if($result) {
                $this->addMessage('success', 'Uživatel byl přidán');
            } else {
                $this->uzivatelData = ['admin' => $admin, 'login' => $login, 'jmeno' => $jmeno, 'datum_narozeni' => $datumNarozeni];

                $result = $dbCor->executeQuery('SELECT 1 FROM uzivatele WHERE login = :login OR (jmeno = :jmeno AND datum_narozeni = :datum_narozeni) OR (jmeno = :jmeno AND datum_narozeni IS NULL)',
                        ['login' => $login, 'jmeno' => $jmeno, 'datum_narozeni' => $datumNarozeni]);

                if($result) {
                    $this->addMessage('danger', 'Uživatele se nepodařilo přidat - uživatel se zadaným přihlašovacím jménem nebo jménem a datem narození již existuje');
                } else {
                    $this->addMessage('danger', 'Uživatele se nepodařilo přidat');
                }
            }
        } else {
            $this->uzivatelData = ['admin' => $admin, 'login' => $login, 'jmeno' => $jmeno, 'datum_narozeni' => $datumNarozeni];
            
            $this->addMessage('danger', 'Uživatele se nepodařilo přidat - nejsou správně vyplněny všechny požadované položky');
        }
    }
    
    protected function actionDelete($actionDelete, $dbCor) {
        $uzivatelId = array_keys($actionDelete)[0];

        // uživatele lze smazat pouze, pokud existuje jiný uživatel, který je administrátor
        $result = $dbCor->executeQuery('DELETE FROM uzivatele'
                . ' WHERE uzivatel_id = :uzivatel_id AND true = (SELECT DISTINCT true FROM uzivatele WHERE admin AND uzivatel_id != :uzivatel_id2)',
                ['uzivatel_id' => $uzivatelId, 'uzivatel_id2' => $uzivatelId]);
        
        if($result) {
            $this->addMessage('success', 'Uživatel byl smazán');
        } else {
            $this->addMessage('danger', 'Uživatele se nepodařilo smazat');
        }
    }
    
    protected function actionLoadEdit($actionLoadEdit, $dbCor) {
        $uzivatelId = array_keys($actionLoadEdit)[0];

        $result = $dbCor->executeQuery('SELECT * FROM uzivatele WHERE uzivatel_id = :uzivatel_id', ['uzivatel_id' => $uzivatelId]);
        
        if($result) {
            $this->uzivatelData = ['uzivatel_id' => $result[0]['uzivatel_id'], 'admin' => $result[0]['admin'], 'login' => $result[0]['login'], 'jmeno' => $result[0]['jmeno'], 
                'datum_narozeni' => $result[0]['datum_narozeni']];
        }
    }
    
    protected function actionEdit($actionEdit, $dbCor) {
        $uzivatelId = array_keys($actionEdit)[0];
        
        $admin = CommonFunc::safePOST('admin');
        $login = CommonFunc::safePOST('login');
        $heslo = CommonFunc::safePOST('heslo');
        $datumNarozeni = CommonFunc::safePOST('datum_narozeni');
        $datumNarozeni = $datumNarozeni ? (new \DateTime(str_replace(' ', '', $datumNarozeni)))->format('Y-m-d') : null;
        $jmeno = CommonFunc::safePOST('jmeno');

        $dbCor->beginTransaction();
        
        $result = (boolean) $dbCor->executeQuery('UPDATE uzivatele SET login = :login, jmeno = :jmeno, datum_narozeni = :datum_narozeni WHERE uzivatel_id = :uzivatel_id',
                ['jmeno' => $jmeno, 'login' => $login, 'datum_narozeni' => $datumNarozeni, 'uzivatel_id' => $uzivatelId]);
        
        if($heslo!=='') {
            $hesloHash = password_hash($heslo, PASSWORD_BCRYPT);
            $result &= (boolean) $dbCor->executeQuery('UPDATE uzivatele SET heslo = :heslo WHERE uzivatel_id = :uzivatel_id',
                    ['heslo' => $hesloHash, 'uzivatel_id' => $uzivatelId]);
        }
        
        if($result) {
            $dbCor->commitTransaction();
            $this->addMessage('success', 'Uživatel byl upraven');
        } else {
            $dbCor->rollbackTransaction();
            $this->addMessage('danger', 'Uživatele se nepodařilo upravit');
        }
    }
    
    public function render() {
        $renderer = new \ScriptsSC\Renderer();
        $renderer->addParam('presenter', $this);
        $renderer->setLayout('uzivatele');
        $renderer->render();
    }
    
    protected function loadUzivatele($dbCor) {
        $this->administratori = $dbCor->executeQuery('SELECT * FROM uzivatele WHERE admin ORDER BY jmeno, vytvoreno DESC');
        $this->pacientiOtevrenyUcet = $dbCor->executeQuery('SELECT * FROM uzivatele WHERE NOT admin AND aktivni ORDER BY jmeno DESC, vytvoreno DESC');
        $this->pacientiUzavrenyUcet = $dbCor->executeQuery('SELECT u.*, v.cas AS distest_cas FROM uzivatele AS u LEFT JOIN vysledky AS v'
                . ' ON u.uzivatel_id = v.uzivatel_id WHERE NOT admin AND NOT aktivni ORDER BY jmeno, vytvoreno DESC');
    }
    
    public function getUzivatelData($key = null) {
        if(!$key) {
            return $this->uzivatelData;
        } else {
            if($this->uzivatelData) {
                if(isset($this->uzivatelData[$key])) {
                    return $this->uzivatelData[$key];
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }
    
    public function getAdministratori() {
        if(is_array($this->administratori)) {
            return $this->administratori;
        } else {
            return [];
        }
    }
    
    public function getPacientiOtevrenyUcet() {
        if(is_array($this->pacientiOtevrenyUcet)) {
            return $this->pacientiOtevrenyUcet;
        } else {
            return [];
        }
    }
    
    public function getPacientiUzavrenyUcet() {
        if(is_array($this->pacientiUzavrenyUcet)) {
            return $this->pacientiUzavrenyUcet;
        } else {
            return [];
        }
    }
}
