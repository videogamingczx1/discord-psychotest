<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/APresenter.php';
require_once __DIR__ . '/UlozenPresenter.php';

class LoginPresenter extends APresenter
{
  
    public function __construct() {
        $actionSend = CommonFunc::safePOST('send');
        if($actionSend) {
            $this->actionSend();
        } else {
           // $this->render(); 
        }
    }
    
    protected function actionSend() {
        $dbCor = $this->getOpenDatabaseConnection();
        $login = CommonFunc::safePOST('login');
        $heslo = CommonFunc::safePOST('heslo');
        
        $result = $dbCor->executeQuery('SELECT * FROM uzivatele WHERE login = :login', ['login' => $login]);
        
        if($result && password_verify($heslo, $result[0]['heslo'])) {
            $_SESSION['uzivatel'] = $result[0];
            if(!$result[0]['aktivni']) {
                // Uživatelův účet je neaktivní, načíst a zobrazit výsledky dotazníku
                $ulozenPresenter = new UlozenPresenter();
                $ulozenPresenter->loadVysledkyOdpovediByUzivatelId($dbCor, $_SESSION['uzivatel']['uzivatel_id']);
                $ulozenPresenter->render(); // Předpokládá, že UlozenPresenter správně nastaví a zobrazí šablonu
                exit;
            }
            App::reload(); // Přesměrování na domovskou stránku nebo dashboard
        } else {
            $this->addMessage('danger', 'Chybné přihlašovací údaje');
           // $this->render(); // Znovu zobrazit přihlašovací formulář s chybovou zprávou
        }
    }
    
    public function render() {
        $renderer = new \ScriptsSC\Renderer();
        $renderer->addParam('presenter', $this);
        $renderer->setLayout('login');
        $renderer->render();
    }  
}
