<?php
namespace DiscordPsychotesty;

require_once __DIR__ . '/APresenter.php';

class ADiscordPresenter extends APresenter
{
    protected $kategorie;
    protected $otazky;
    protected $template;
    protected $vysledek = [];
    protected $opravneniSkore = [];
    
    public function loadKategorieAndOtazky($dbCor) {
        $this->kategorie = $dbCor->executeQuery('SELECT * FROM kategorie ORDER BY kategorie_id');
        
        $otazky = $dbCor->executeQuery('SELECT * FROM otazky ORDER BY kategorie_id, poradi');
        
        $this->otazky = [];
        
        foreach($otazky as $otazka) {
            if(!isset($this->otazky[$otazka['kategorie_id']])) {
                $this->otazky[$otazka['kategorie_id']] = [];
            }
            $this->otazky[$otazka['kategorie_id']][] = $otazka;
        }
    }
    
    public function render() {
        $renderer = new \ScriptsSC\Renderer();
        $renderer->addParam('presenter', $this);
        $renderer->setLayout($this->template);
        $renderer->render();
    }
    
    public function getKategorie() {
        if(is_array($this->kategorie)) {
            return $this->kategorie;
        } else {
            return [];
        }
    }

    public function loadVysledkyOdpovediByUzivatelId($dbCor, $uzivatelId) {
        $sql = "SELECT vo.otazka_id, vo.odpoved
            FROM public.vysledky_odpovedi AS vo
            JOIN public.vysledky AS v ON vo.vysledek_id = v.vysledek_id
            WHERE v.uzivatel_id = :uzivatel_id";

        $vysledkyOdpovedi = $dbCor->executeQuery($sql, ['uzivatel_id' => $uzivatelId]);
        
        $this->vysledek = [];
        
        foreach($vysledkyOdpovedi as $odpoved) {
            $this->vysledek[$odpoved['otazka_id']] = $odpoved['odpoved'];
        }
    }
    
    public function getOtazky($kategorie) {
        if(isset($this->otazky[$kategorie])) {
            return $this->otazky[$kategorie];
        } else {
            return [];
        }
    }


    
    protected $otazkyTexty = [
        1 => ['nazev' => 'Zobrazit kanály', 'popis' => 'Umožní členům zobrazovat kanály (kromě soukromých kanálů).'],
        2 => ['nazev' => 'Správa kanálů', 'popis' => 'Umožní členům vytvářet, upravovat nebo odstraňovat kanály.'],
        3 => ['nazev' => 'Správa rolí', 'popis' => 'Umožní členům vytvářet nové role a upravovat nebo odstraňovat role nižší než jejich nejvyšší role. Zároveň členům umožní měnit oprávnění jednotlivých kanálů, k nimž mají přístup.'],
        4 => ['nazev' => 'Vytvářet výrazy', 'popis' => 'Umožní členům přidávat vlastní smajlíky, samolepky a zvuky na tomto serveru.'],
        5 => ['nazev' => 'Spravovat výrazy', 'popis' => 'Umožní členům upravovat nebo odebírat vlastní smajlíky, samolepky a zvuky na tomto serveru.'],
        6 => ['nazev' => 'Zobrazit Audit Log', 'popis' => 'Umožní členům zobrazit záznam toho, kdo na tomto serveru provedl jaké změny.'],
        7 => ['nazev' => 'Správa háčků webhook', 'popis' => 'Umožní členům vytvářet, upravovat nebo odstraňovat webhooky, které mohou na tomto serveru zveřejňovat zprávy z jiných aplikací nebo stránek.'],
        8 => ['nazev' => 'Správa serveru', 'popis' => 'Umožni členům měnit název tohoto serveru, přepínat oblasti, zobrazovat všechny pozvánky, přidávat na tento server boty a vytvářet a aktualizovat pravidla funkce AutoMod.'],
        9 => ['nazev' => 'Vytvořit pozvánku', 'popis' => 'Umožní členům zvát na tento server nové lidi.'],
        10 => ['nazev' => 'Změna přezdívky', 'popis' => 'Umožní členům měnit svou přezdívku, jméno přizpůsobené pouze pro tento server.'],
        11 => ['nazev' => 'Správa přezdívek', 'popis' => 'Umožní členům měnit přezdívky ostatních členů.'],
        12 => ['nazev' => 'Vyhodit člena', 'popis' => 'Umožní členům odebírat z tohoto serveru jiné členy. Vyhození členové se budou moci opět přidat, pokud dostanou novou pozvánku.'],
        13 => ['nazev' => 'Zabanovat členy', 'popis' => 'Umožňuje členům trvale zabanovat ostatní členy z tohoto serveru a odstranit související historie zpráv.'],
        14 => ['nazev' => 'Dát členům pauzu', 'popis' => 'Když uživatele dočasně zablokuješ, nebude moct posílat zprávy v chatu, odpovídat ve vláknech, reagovat na zprávy ani mluvit v hlasových nebo řečnických kanálech.'],
        15 => ['nazev' => 'Odesílat zprávy', 'popis' => 'Umožní členům posílat zprávy do textových kanálů.'],
        16 => ['nazev' => 'Odesílat zprávy ve vláknech', 'popis' => 'Umožní členům posílat zprávy ve vláknech.'],
        17 => ['nazev' => 'Vytvářet veřejná vlákna', 'popis' => 'Umožní členům vytvářet vlákna viditelná pro všechny v kanálu.'],
        18 => ['nazev' => 'Vytvářet soukromá vlákna', 'popis' => 'Umožní členům vytvářet vlákna pouze pro zvané.'],
        19 => ['nazev' => 'Vkládat odkazy', 'popis' => 'Odkazy sdílené členy do textových kanálů mohou zobrazovat vložený obsah.'],
        20 => ['nazev' => 'Přiložit soubory', 'popis' => 'Umožní členům sdílet do textových kanálů soubory nebo média.'],
        21 => ['nazev' => 'Přidat reakce', 'popis' => 'Umožní členům přidávat nové emoji reakce na zprávy. Pokud je tato možnost vypnutá, členové stále mohou využívat k reakcím jakékoliv stávající smajlíky.'],
        22 => ['nazev' => 'Používat externí smajlíky', 'popis' => 'Umožní členům s Discordem Nitro používat smajlíky z jiných serverů.'],
        23 => ['nazev' => 'Používat externí samolepky', 'popis' => 'Umožní členům s Discordem Nitro používat samolepky z jiných serverů.'],
        24 => ['nazev' => 'Zmiňovat @everyone, @here a všechny role', 'popis' => 'Umožní členům používat @everyone (všichni na serveru) a @here (pouze online členové v daném kanálu). Mohou také @zmiňovat všechny role a to i role s vypnutým povolením, aby je mohl zmínit kdokoliv.'],
        25 => ['nazev' => 'Správa zpráv', 'popis' => 'Umožní členům odstraňovat zprávy od jiných členů nebo jakoukoliv zprávu připnout.'],
        26 => ['nazev' => 'Správa vlákna', 'popis' => 'Umožňuje členům přejmenovávat vlákna, odstraňovat je, zavírat a zapínat pro ně pomalý režim. Členové taky můžou zobrazovat soukromá vlákna.'],
        27 => ['nazev' => 'Číst historii zpráv', 'popis' => 'Umožní členům číst starší zprávy odeslané v kanálech. Pokud je tato možnost vypnutá, uvidí členové odeslané zprávy pouze když jsou online a mají kurzor nad daným kanálem.'],
        28 => ['nazev' => 'Odesílat zprávy převodem textu na řeč', 'popis' => 'Umožní členům posílat zprávy převedené z textu na řeč pomocí zprávy začínající na /tts. Tyto zprávy uslyší všichni uživatelé, kteří si prohlížejí daný kanál.'],
        29 => ['nazev' => 'Používat příkazy aplikací', 'popis' => 'Umožní členům používat příkazy z aplikací včetně /příkazů a příkazů kontextové nabídky.'],
        30 => ['nazev' => 'Odesílat hlasové zprávy', 'popis' => 'Umožňuje členům odesílat hlasové zprávy.'],
        31 => ['nazev' => 'Připojit', 'popis' => 'Umožní členům přidávat se do hlasových kanálů a poslouchat ostatní.'],
        32 => ['nazev' => 'Mluvit', 'popis' => 'Umožní členům hovořit v hlasových kanálech. Pokud je tato možnost vypnutá, jsou členové automaticky ztlumení, dokud jim ztlumení nezruší někdo s oprávněním „Vypnout členům mikrofon“.'],
        33 => ['nazev' => 'Video', 'popis' => 'Umožní členům na tomto serveru sdílet video, obrazovku nebo streamovat hru.'],
        34 => ['nazev' => 'Použít aktivity', 'popis' => 'Umožní členům používat aktivity na tomto serveru.'],
        35 => ['nazev' => 'Použít soundboard', 'popis' => 'Umožní členům posílat zvuky ze soundboardu serveru. Zjistit více.'],
        36 => ['nazev' => 'Používat externí zvuky', 'popis' => 'Umožní členům s Discordem Nitro používat zvuky z jiných serverů.'],
        37 => ['nazev' => 'Používat hlasovou aktivitu', 'popis' => 'Umožní členům hovořit v hlasových kanálech, jakmile začnou mluvit. Pokud je tato možnost vypnutá, musí používat funkci Push-to-talk. Je to dobré k omezení zvuků na pozadí nebo hlučných uživatelů.'],
        38 => ['nazev' => 'Přednostní mluvčí', 'popis' => 'Členové jdou v hlasových kanálech lépe slyšet. Pokud je tato možnost zapnutá, hlasitost ostatních bez tohoto oprávnění se automaticky ztlumí. Funkci přednostního mluvčího lze aktivovat pomocí klávesové zkratky Push to Talk (Přednostní).'],
        39 => ['nazev' => 'Vypnout členům mikrofon', 'popis' => 'Umožní členům v hlasových kanálech ztlumit jiné členy pro všechny ostatní.'],
        40 => ['nazev' => 'Vypnout členům zvuk', 'popis' => 'Umožní členům ztlumit zvuk jiným členům v hlasových kanálech, takže nebudou moci mluvit s ostatními ani je poslouchat.'],
        41 => ['nazev' => 'Přesunout uživatele', 'popis' => 'Umožní členům odpojovat nebo přesouvat jiné členy mezi hlasovými kanály, k nimž má člen s tímto oprávněním přístup.'],
        42 => ['nazev' => 'Nastavení stavu hlasového kanálu', 'popis' => 'Umožní členům vytvářet a upravovat stavy hlasových kanálů.'],
        43 => ['nazev' => 'Vytvářet události', 'popis' => 'Umožní členům vytvářet události.'],
        44 => ['nazev' => 'Správa událostí', 'popis' => 'Umožní členům upravovat nebo rušit události.']
        ];

    

    
    public function getOtazkyTexty() {
        return $this->otazkyTexty;
    }
    


 
//Prevence před tím, aby když uživatel bude dávat stále ano, tak aby nebylo vše povoleno

    protected function vyhodnotOpravneni() {
        $skoreOdpovedi = [1 => 5, 2 => 4, 3 => 3, 4 => 2, 5 => 1]; 
        $this->opravneniSkore = []; 
        $hodnotyOpravneni = []; 
    
        foreach ($this->vysledek as $otazkaId => $odpoved) {
            if (isset($skoreOdpovedi[$odpoved])) {
               
                $skore = $skoreOdpovedi[$odpoved];
        
               
                if (isset($this->otazkyTexty[$otazkaId])) {
                    if (!isset($hodnotyOpravneni[$otazkaId])) {
                        $hodnotyOpravneni[$otazkaId] = 0;
                    }
                    $hodnotyOpravneni[$otazkaId] += $skore;
                }
            }
        }
    
      
        foreach ($hodnotyOpravneni as $opravneniId => $skore) {
            
            if ($skore > 20) {
                $this->opravneniSkore[$opravneniId] = true;
            } else {
                $this->opravneniSkore[$opravneniId] = false;
            }
        }
    }
    
    public function getTextOtazky($otazkaId) {
        $otazky = [
            1 => "Umožní členům zobrazovat kanály (kromě soukromých kanálů).",
            
        ];

        
        return $otazky[$otazkaId] ?? 'Neznámá otázka';
    }


    
}
