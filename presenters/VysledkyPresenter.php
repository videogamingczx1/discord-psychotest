<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/ADiscordPresenter.php';

class VysledkyPresenter extends ADiscordPresenter
{
    protected $vysledkyForSelect;
    protected $vysledek;
    
    public function __construct() {
        $this->template = 'vysledky';
        
        $dbCor = $this->getOpenDatabaseConnection();
        
        $akce = CommonFunc::safePOST('akce');
        
        if($akce == 'vysledek') {
            $this->template = 'vysledky_vysledek';
            
            $this->loadKategorieAndOtazky($dbCor);

            $this->loadVysledek($dbCor);
        } else if($akce == 'vyhledat') {
            $vyhledat = CommonFunc::safePOST('vyhledat');
            
            $this->loadVysledkyForSelect($dbCor, $vyhledat);
            
            echo json_encode($this->vysledkyForSelect);
            
            exit();
        } else {
            $this->template = 'vysledky';
            
            $this->loadVysledkyForSelect($dbCor);
        }
        
        $dbCor->closeConnection();
    }
    
    public function akceVysledek() {
        
    }
    
    public function loadVysledkyForSelect($dbCor, $vyhledat = null) {
        $innerQuery = "SELECT v.vysledek_id, u.jmeno || CASE WHEN u.datum_narozeni IS NOT NULL THEN ' (* ' || TO_CHAR(u.datum_narozeni, 'DD. MM. YYYY') || ')' ELSE '' END AS vysledek_popis"
                . ", v.cas"
                . " FROM vysledky AS v JOIN uzivatele AS u ON v.uzivatel_id = u.uzivatel_id";
        if($vyhledat) {
            $vysledky = $dbCor->executeQuery("SELECT * FROM (" . $innerQuery . ") AS s"
                    . " WHERE vysledek_popis LIKE :vyhledat ORDER BY cas DESC",
                    ['vyhledat' => '%' . $vyhledat . '%']);
        } else {
            $vysledky = $this->vysledkyForSelect = $dbCor->executeQuery($innerQuery);
        }
        $this->vysledkyForSelect = [];
        foreach($vysledky as $vysledek) {
            $this->vysledkyForSelect[$vysledek['vysledek_id']] = $vysledek['vysledek_popis'];
        }
    }
    
    public function loadVysledek($dbCor) {
        if(CommonFunc::safePOST('vysledek')) {
            $vysledek = $dbCor->executeQuery('SELECT * FROM vysledky_odpovedi WHERE vysledek_id = :vysledek_id', ['vysledek_id' => CommonFunc::safePOST('vysledek')]);
            
            $this->vysledek = [];

            foreach($vysledek as $v) {
                $this->vysledek[$v['otazka_id']] = $v['odpoved'];
            }
        } else {
            $this->vysledek = null;
        }
    }
    
    public function getVysledkyForSelect() {
        return $this->vysledkyForSelect;
    }
    
    public function getVysledek() {
        return $this->vysledek;
    }
}
