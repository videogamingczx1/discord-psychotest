<?php
namespace DiscordPsychotesty;

use ScriptsSC\CommonFunc;

require_once __DIR__ . '/ADiscordPresenter.php';

class UlozenPresenter extends ADiscordPresenter
{
    protected $vysledek;
    protected $kategorie;
    protected $opravneniSkore = [];
    public function __construct() {
        $this->template = 'prava';
        $dbCor = $this->getOpenDatabaseConnection();
        $this->loadKategorieAndOtazky($dbCor);
        $dbCor = $this->getOpenDatabaseConnection();
        $uzivatelId = App::getUzivatelId();
        $this->loadVysledkyOdpovediByUzivatelId($dbCor, $uzivatelId);
        $dbCor->closeConnection();
        $this->vyhodnotOpravneni();
    }


    
    public function getVysledek() {
        return $this->vysledek;
    }
    
  
}
