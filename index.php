<?php
namespace DiscordPsychotesty;

require_once __DIR__ . '/scripts_internet/bootstrap.php';

require_once __DIR__ . '/presenters/DiscordPresenter.php';
require_once __DIR__ . '/presenters/LoginPresenter.php';
require_once __DIR__ . '/presenters/StatistikaPresenter.php';
require_once __DIR__ . '/presenters/UzivatelePresenter.php';
require_once __DIR__ . '/presenters/VysledkyPresenter.php';
require_once __DIR__ . '/presenters/UlozenPresenter.php';

use ScriptsSC\CommonFunc;
use ScriptsSC\CustomDatabaseConnector;

class App 
{   
    public function __construct() {
        session_start();
        
        if(CommonFunc::safePOST('logout')) {
            self::logout();
        }

        $dbCor = self::getDatabaseConnection();

        if(isset($_SESSION['uzivatel'])) {
            // Kontrola, zda má uživatel neaktivní účet
            if(!$this->jeUcetAktivni($dbCor, $_SESSION['uzivatel']['uzivatel_id']) && !$_SESSION['uzivatel']['admin']) {
              
                $ulozenPresenter = new UlozenPresenter();
                $ulozenPresenter->render();
            } else {
               
                if($_SESSION['uzivatel']['admin']) {
                   
                    $this->handleAdminRoutes(CommonFunc::safeGET('page'));
                } else {
                
                    $distest = new DiscordPresenter();
                    $distest->render();
                }
            }
        } else {
            // Pro nepřihlášené uživatele zobrazit přihlašovací formulář
            $login = new LoginPresenter();
            $login->render();
        }
    }

    private function jeUcetAktivni($dbCor, $uzivatelId) {
        $uzivateleInfo = $dbCor->executeQuery('SELECT aktivni FROM uzivatele WHERE uzivatel_id = :uzivatel_id', ['uzivatel_id' => $uzivatelId]);
        if (!empty($uzivateleInfo) && is_array($uzivateleInfo)) {
            return $uzivateleInfo[0]['aktivni'];
        }
        return false; // Implicitně považovat účet za neaktivní, pokud není nalezen v DB
    }
    
    private function handleAdminRoutes($page) {
        switch($page) {
            case 'statistika':
                $statistika = new StatistikaPresenter();
                $statistika->render();
                break;
            case 'vysledky':
                $vysledky = new VysledkyPresenter();
                $vysledky->render();
                break;
            default:
                $uzivatele = new UzivatelePresenter();
                $uzivatele->render();
                break;
        }
    }
    
    public static function getDatabaseConnection() {
        return CustomDatabaseConnector::getOpenPgSQLPDOConnector('10.40.20.206', 5436, 'JménoDatabáze', 'VášUživatel', 'VašeHeslo');
    }
    
    public static function getUzivatelId() {
        return $_SESSION['uzivatel']['uzivatel_id'];
    }
    
    public static function logout($redirect = true) {
        session_destroy();
        unset($_SESSION);
        if($redirect) {
            self::reload();
        }
    }
    
    public static function reload() {
        header('Location: ' . $_SERVER['PHP_SELF']);
    }
}

new App();
?>